# -*- coding: utf-8 -*-

import json
import math
import socket
import sys
import traceback


# Test tracks available:
FINLAND_TRACK_NAME = 'keimola'
GERMANY_TRACK_NAME = 'germany'
USA_TRACK_NAME = 'usa'


class Race(object):

    def __init__(self, data):
        self.track = Track(data.get('track', {}))
        self.cars = [Car(d) for d in data.get('cars', [])]
        #self.race_session

    def get_car_by_id(self, car_id):
        for car in self.cars:
            if car.id == car_id:
                return car


class Car(object):

    def __init__(self, data):
        self.id = data.get('id')
        self.length = data.get('length')
        self.width = data.get('width')
        self.guide_flag_position = data.get('guideFlagPosition')


class Track(object):

    def __init__(self, data):
        self.id = data.get('id')
        self.name = data.get('name')
        self.pieces = [Piece(d) for d in data.get('pieces', [])]
        self.lanes = [Lane(d) for d in data.get('lanes', [])]

    def get_piece(self, index):
        return self.pieces[index % len(self.pieces)]


class Piece(object):

    def __init__(self, data):
        self.length = data.get('length')
        self.switch = data.get('switch')
        self.radius = data.get('radius') # radius at center line of the track
        self.angle = data.get('angle')

    def get_lane_length(self, lane):
        if self.length is not None:
            return self.length
        else:
            if self.angle < 0: # left turns
                radius = self.radius + lane.distance_from_center
            else: # right turns
                radius = self.radius - lane.distance_from_center
            return ((2.0 * math.pi * radius) / 360.0) * abs(self.angle)


class Lane(object):

    def __init__(self, data):
        self.distance_from_center = data.get('distanceFromCenter')
        self.index = data.get('index')


class Position(object):

    def __init__(self, data):
        self.angle = data.get('angle')
        piece_position = data.get('piecePosition', {})
        self.piece_index = piece_position.get('pieceIndex')
        self.in_piece_distance = piece_position.get('inPieceDistance')
        lane = piece_position.get('lane', {})
        self.start_lane_index = lane.get('startLaneIndex')
        self.end_lane_index = lane.get('endLaneIndex')
        self.lap = piece_position.get('lap')

    def __unicode__(self):
        return u'Position {{angle: {0}, pieceIndex: {1}, inPieceDistance: {2}}}'.format(self.angle, self.piece_index, self.in_piece_distance)


class YaalBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.car_id = None
        self.race = None
        self.crashed = False
        self.last_position = None
        self.last_game_tick = None

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        return self.msg('joinRace',data={
            'botId': {'name': self.name, 'key': self.key},
            'trackName': GERMANY_TRACK_NAME,
            'password': 'yaal',
            'carCount': 2,
        })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join() # solo
        #self.join_race() # multi-joueurs avec choix du circuit
        self.msg_loop()

    def on_join(self, data, game_tick=None):
        print("Joined")
        self.ping()

    def on_your_car(self, data, game_tick=None):
        self.car_id = data
        self.ping()

    def on_game_init(self, data, game_tick=None):
        self.race = Race(data.get('race', {}))
        #print(data['race']['track']['pieces'])
        #print(data['race']['track']['lanes'])
        #print(data['race']['track']['startingPoint'])
        # => keimola_game.txt
        self.ping()

    def on_game_start(self, data, game_tick=None):
        print("Race started")
        self.ping()

    def compute_speed(self, current_position, current_game_tick):
        """ Vitesse = distance entre les deux positions / nombre de ticks les séparant """
        if (
                self.last_position is None or 
                self.last_game_tick is None or 
                current_game_tick is None or
                current_game_tick == self.last_game_tick
                ):
            return 0
        distance = self.compute_distance(self.last_position, current_position)
        return distance / (current_game_tick - self.last_game_tick)

    def compute_distance(self, position_1, position_2):
        """ La distance est calculé sur la voie courante.
        Pour l'instant on suppose qu'il n'y a pas de changement de voie. """
        if position_1.piece_index == position_2.piece_index:
            return position_2.in_piece_distance - position_1.in_piece_distance
        else:
            piece = self.race.track.get_piece(position_1.piece_index)
            lane = self.race.track.lanes[position_1.start_lane_index]
            return (piece.get_lane_length(lane) - position_1.in_piece_distance +
                    position_2.in_piece_distance)

    def on_car_positions(self, data, game_tick=None):
        if self.crashed:
            self.ping()
            return

        for position_data in data:
            if position_data.get('id') == self.car_id:
                current_position = Position(position_data)

        speed = self.compute_speed(current_position, game_tick)
        # remarque : une accélération constate de 0.5 trend vers une vitesse de 5
        # La vitesse maximale semble donc être 10

        current_piece = self.race.track.get_piece(current_position.piece_index)
        next_piece = self.race.track.get_piece(current_position.piece_index + 1)
        if not current_piece.angle and not next_piece.angle:
            # à fond dans la ligne droite
            throttle = 1
        else:
            # ralentissement jusqu'à la vitesse 5
            throttle = 0.5 if speed < 7 else 0.1
        #print(u'{0}, throttle: {1}, gameTick: {2}, speed: {3}'.format(unicode(current_position), throttle, game_tick, speed))

        self.last_position = current_position 
        self.last_game_tick = game_tick
        self.throttle(throttle)

    def on_turbo_available(self, data, game_tick=None):
        print("Turbo available : {0}".format(repr(data)))
        # exemple :
        # {u'turboDurationTicks': 30, u'turboFactor': 3.0, u'turboDurationMilliseconds': 500.0}
        self.ping()

    def on_crash(self, data, game_tick=None):
        print("Someone crashed (game tick: {0})".format(game_tick))
        if data == self.car_id:
            self.crashed = True
        self.ping()

    def on_spawn(self, data, game_tick=None):
        print("Someone spawned (game tick: {0})".format(game_tick))
        if data == self.car_id:
            self.crashed = False
        self.ping()

    def on_game_end(self, data, game_tick=None):
        print("Race ended")
        self.ping()

    def on_error(self, data, game_tick=None):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                try:
                    msg_map[msg_type](data, game_tick=msg.get('gameTick'))
                except:
                    traceback.print_exc(limit=5, file=sys.stdout)
                    self.ping()
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = YaalBot(s, name, key)
        bot.run()
